﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Appium.Service;
using OpenQA.Selenium.Appium.Service.Options;
using System.Runtime;
using System.Diagnostics;

namespace sodalis_iOS
{

	[TestFixture]
	public class InitDriver: TestBase
	{
        

		[SetUp]
		public void Setup()
		{
			var currentTestname = TestContext.CurrentContext.Test.Name;
			SubTestFolder = Directory.CreateDirectory(Path.Combine(LogsFolder.FullName, currentTestname));
            TestContext.Out.WriteLine("\nSTART of Test : " + TestContext.CurrentContext.Test.Name);
            _iPad.LaunchApp();
            _iPhone.LaunchApp();

            // if after -restart of the app the login is shown, the app should be closed and restarted again
            try
            {
                _iPad.FindElementById("_username");
                _iPad.CloseApp();
                _iPad.LaunchApp();
            }
            catch(Exception){}
             

            try
            {
                _iPhone.FindElementById("_username");
                _iPhone.CloseApp();
                _iPhone.LaunchApp();
            }
            catch(Exception){}
		}

		[TearDown]
        public void Teardown()
        {
            // var Testresult = TestContext.;
            var Testresult = TestContext.CurrentContext.Result.Outcome.Status.ToString();

            var logfilename = TestContext.CurrentContext.Test.Name;
            var WritePath = Path.Combine(SubTestFolder.FullName, logfilename);
            var WritePath2 = WritePath.ToString() + ".txt";
            File.WriteAllText(WritePath2, Testresult);
            TestContext.Out.WriteLine("END od Test : "+TestContext.CurrentContext.Test.Name+" - Testresult: "+Testresult+"\n");
            _iPad.CloseApp();
            _iPhone.CloseApp();

        }

        
		[OneTimeSetUp]
		public void BeforeAllTest()
		{

            string[] files = Directory.GetFiles(@"/Users/Beeware/Jenkins/workspace/iOS_appiumtests/Builds", "*.ipa", SearchOption.AllDirectories);
            string ipa=Path.GetFileName(files[0]);
            logComment("using build : "+ipa);

			LogsFolder = Directory.CreateDirectory(Path.Combine(TestFolder,"Logs"));
            ////server capabilities
            //DesiredCapabilities ServerCap = new DesiredCapabilities();
            //ServerCap.SetCapability("session-override", "true");
            //OptionCollector args = new OptionCollector().AddCapabilities(ServerCap);

            /// This area can be used to start the Appium server programmatically
            /// 
            // logComment("Closing old appium server sessions");
            // closeOldSessions();
            // System.Threading.Thread.Sleep(4000);
            // logComment("Starting new appium server session");
            // _service.Start();

			// start appium server
            // for debugging it could be good to start the appium server in a terminal window and disable this line
			// use "Appium -a 127.0.0.1 -p 4723" for starting the local server in terminal



			//ipad capabilities
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.SetCapability("xcodeOrgId", "5RUCMEBU79");
			cap.SetCapability("xcodeSigningId", "iPhone Developer");
			cap.SetCapability("udid", "a6c2fb810e897e30ae344ed6cd9c733f591de70e");
			cap.SetCapability("deviceName", "a6c2fb810e897e30ae344ed6cd9c733f591de70e");
			cap.SetCapability("platformVersion", "11.3.1");
			cap.SetCapability("platformName", "iOS");
			// cap.SetCapability("app", "/Users/Beeware/Desktop/Beeware.Sodalis.Mobile.iOS.ipa");
            cap.SetCapability("app", "/Users/Beeware/Jenkins/workspace/iOS_appiumtests/Builds/"+ipa);
			cap.SetCapability("newCommandTimeout", 600000);
			cap.SetCapability("automationName", "XCUITest");
			cap.SetCapability("fullReset", false);
            cap.SetCapability("noReset", true);
			cap.SetCapability("clearSystemFiles", true);

            // initialize appium driver for ipad
            logComment("Initializing webdriver for ipad");
            _iPad = new DrvWithName(new Uri("http://127.0.0.1:4723/wd/hub"), cap, TimeSpan.FromMinutes(3), "iPad");
         
			// iphone capabilities
			DesiredCapabilities cap2 = new DesiredCapabilities();
			cap2.SetCapability("xcodeOrgId", "5RUCMEBU79");
			cap2.SetCapability("xcodeSigningId", "iPhone Developer");
			cap2.SetCapability("udid", "28434f765620d9d0fa28164e82bef55edca65e08");
			cap2.SetCapability("deviceName", "28434f765620d9d0fa28164e82bef55edca65e08");
			cap2.SetCapability("platformVersion", "11.3.1");
			cap2.SetCapability("platformName", "iOS");
            cap2.SetCapability("app", "/Users/Beeware/Jenkins/workspace/iOS_appiumtests/Builds/" + ipa);
			cap2.SetCapability("newCommandTimeout", 600000);

			cap2.SetCapability("automationName", "XCUITest");
			cap2.SetCapability("fullReset", false);
			cap2.SetCapability("noReset", true);
			cap2.SetCapability("clearSystemFiles", true);
            cap2.SetCapability("wdaLocalPort", 8200);

            // initialize appium driver for iphone
            logComment("Initializing webdriver for iphone");
            _iPhone = new DrvWithName(new Uri("http://127.0.0.1:4723/wd/hub"), cap2, TimeSpan.FromMinutes(3), "iPhone");


			// login on iPad and iphone
            logComment("Login to Sodalis");
			findId("_username", 15, _iPad).Click();
			findId("_username", 15, _iPhone).Click();
  			findId("_username", 15, _iPad).Clear();
			findId("_username", 15, _iPhone).Clear();
   			findId("_username", 15, _iPad).SendKeys("owner@ios.de");
			findId("_username", 15, _iPhone).SendKeys("user1@ios.de");
            findId("_password", 15, _iPad).Click();
			findId("_password", 15, _iPhone).Click();
            findId("_password", 15, _iPad).Clear();
			findId("_password", 15, _iPhone).Clear();
            findId("_password", 15, _iPad).SendKeys("x99sFUV!");
			findId("_password", 15, _iPhone).SendKeys("x99sFUV!");
            findId("Go", 15, _iPad).Click();
			findId("Go", 15, _iPhone).Click();
            
            // accept permission dialogs
            logComment("Accept permission dialogs");
			//findId("//*[@label='Allow' or @label='OK']", 15, _iPad, "xpath").Click();
            //findId("//*[@label='Erlauben' or @label='OK']", 5, _iPhone, "xpath").Click();
            //findId("//*[@label='Allow' or @label='OK']", 5, _iPad, "xpath").Click();
            //findId("//*[@label='Erlauben' or @label='OK']", 5, _iPhone, "xpath").Click();
            System.Threading.Thread.Sleep(8000);
            _iPad.SwitchTo().Alert().Accept();
            _iPhone.SwitchTo().Alert().Dismiss();  // DISMISS IS AN ACCEPT ON IPHONE . .ACCEPT IS NOT HANDLED CORRECTY ON IPHONE WHEN USING .ACCEPT
            _iPad.SwitchTo().Alert().Accept();
            _iPhone.SwitchTo().Alert().Dismiss();  // DISMISS IS AN ACCEPT ON IPHONE . .ACCEPT IS NOT HANDLED CORRECTY ON IPHONE WHEN USING .ACCEPT

            System.Threading.Thread.Sleep(2000);
            _iPad.CloseApp();
            _iPhone.CloseApp();

		}

        [Test]
		public void Calling()
		{
            // starting call
            logComment("Starting a call between iPad and iPhone");
			ValidateLabel("_changeStatusOnUserBtn", 15, _iPad, "Online", "id");
			ValidateLabel("_changeStatusOnUserBtn", 15, _iPhone, "Online", "id");
			findId("//*[@label='User1, iOS']",5,_iPad,"xpath").Click();
			findId("_callButton",15,_iPad).Click();
			findId("_acceptBtn",15,_iPhone).Click();

            // accepting permission dialogs
            logComment("Accept permission dialogs");
            //findId("//*[@label='Allow' or @label='OK']",5,_iPad,"xpath").Click();
            //findId("//*[@label='Erlauben' or @label='OK']",5,_iPhone,"xpath").Click();
            //         findId("//*[@label='Allow' or @label='OK']",5,_iPad,"xpath").Click();
            //findId("//*[@label='Erlauben' or @label='OK']",5,_iPhone,"xpath").Click();
            System.Threading.Thread.Sleep(6000);
            _iPad.SwitchTo().Alert().Accept();
            _iPhone.SwitchTo().Alert().Dismiss();    // DISMISS IS AN ACCEPT ON IPHONE . .ACCEPT IS NOT HANDLED CORRECTY ON IPHONE WHEN USING .ACCEPT
            _iPad.SwitchTo().Alert().Accept();
            _iPhone.SwitchTo().Alert().Dismiss();    // DISMISS IS AN ACCEPT ON IPHONE . .ACCEPT IS NOT HANDLED CORRECTY ON IPHONE WHEN USING .ACCEPT

            // validate streams
            logComment("Validating local and remote streams in Portrait mode on iPad");
			ValidateExist("remoteMediaView", 15, _iPad, "id");
            ValidateExist("localMediaView", 10, _iPad, "id");
            _iPad.Orientation = ScreenOrientation.Landscape;
            logComment("Validating local and remote streams in Landscape mode on iPad");
            ValidateExist("remoteMediaView", 15, _iPad, "id");
            ValidateExist("localMediaView", 10, _iPad, "id");
            _iPad.Orientation = ScreenOrientation.Portrait;
            logComment("Validating local and remote streams in Portrait mode on iPhone");
			ValidateExist("remoteMediaView", 10, _iPhone, "id");
			ValidateExist("localMediaView", 10, _iPhone, "id");
            _iPhone.Orientation = ScreenOrientation.Landscape;
            logComment("Validating local and remote streams in Landscape mode on iPhone");
            ValidateExist("remoteMediaView", 10, _iPhone, "id");
            ValidateExist("localMediaView", 10, _iPhone, "id");
            _iPhone.Orientation=ScreenOrientation.Portrait;
			
            findId("localMediaView", 10, _iPad).Click();
            findId("_endCallButton", 15, _iPad).Click();
        }

		[Test]
        public void text_in_chat()
		{
			// ipad (ios.owner) send chat to iphone (ios.user1)
            logComment("Sending chatmessage from iPad to the iPhone");
			findId("//*[@label='User1, iOS']",18,_iPad,"xpath").Click();
			findId("_messageTextField", 5, _iPad).Click();
			findId("_messageTextField", 5, _iPad).SendKeys("Owner writing");
			findId("_cameraArrowButton", 5, _iPad).Click();
			ValidateLabel("//*[@label='Owner writing']", 5, _iPad, "Owner writing", "xpath");
			findId("//*[@label='Owner, iOS']",5,_iPhone,"xpath").Click();
			ValidateLabel("//*[@label='Owner writing']", 5, _iPhone, "Owner writing", "xpath");

			// iphone (ios.user1) send chat to ipad (ios.owner)
            logComment("Sending chatmessage from iPhone to iPad");
            findId("_messageTextField", 5, _iPhone).Click();
            findId("_messageTextField", 5, _iPhone).SendKeys("User1 writing");
            findId("_cameraArrowButton", 5, _iPhone).Click();
            
			//hiding keyboards
            logComment("Hiding virtual keyboards");
			findId("_nameLabel", 5, _iPhone).Click();
			_iPad.HideKeyboard();
            
			// validate messages
            logComment("Validate Chatmessages existence");
			ValidateLabel("//*[@label='User1 writing']", 5, _iPhone, "User1 writing", "xpath");
			ValidateLabel("//*[@label='User1 writing']", 5, _iPad, "User1 writing", "xpath");
            //findId("//*[@label='Kontakte']", 5, _iPhone, "xpath").Click();
			

   		}

		[Test]
        public void photo_in_chat()
		{
			//// ipad (ios.owner) send photo to iphone (ios.user1)
            logComment("Send photo from iPad to iPhone");
            findId("//*[@label='User1, iOS']", 15, _iPad, "xpath").Click();
			findId("_cameraArrowButton", 5, _iPad, "id").Click();

            // check if camera permission must be given
			try
			{
				//findId("//*[@label='Allow' or @label='OK']", 3, _iPad, "xpath",false).Click();	
                _iPad.SwitchTo().Alert().Accept();
                logComment("Permission dialog was accepted");
			}
			catch(Exception){}

            findId("PhotoCapture", 5, _iPad, "id").Click();
			findId("//*[@label='Use Photo']", 5, _iPad, "xpath").Click();

            // validate on iPhone
            logComment("Validate photo received on iPhone");
			findId("//*[@label='Owner, iOS']", 5, _iPhone, "xpath").Click();
			ValidateExist("(//*[@label='Heruntergeladen'])[" + pictureUploadAmount + "]", 25, _iPhone, "xpath").Click();

			// retrieve all images from chat and click the correct one
            logComment("Open received photo on iPhone");
			var list2 = _iPhone.FindElementsById("_thumbnailImage");
            list2[pictureUploadAmount - 1].Click();
            pictureUploadAmount++;

            // validate opened image
            logComment("Validate opened photo on iPhone");
			ValidateExist("_imageView", 5, _iPhone, "id");
			findId("_closeButton", 5, _iPhone, "id").Click();
			//findId("//*[@label='Kontakte']", 5, _iPhone, "xpath").Click();
        }

		[Test]
        public void picture_in_chat()
		{
			// ipad (ios.owner) send picture to iphone (ios.user1)
            logComment("Sending picture from gallery of iPad");
            findId("//*[@label='User1, iOS']", 15, _iPad, "xpath").Click();
			findId("_addItemButton", 5, _iPad, "id").Click();
			findId("//*[@label='Gallery']", 5, _iPad, "xpath").Click();
			findId("//*[@label='All Photos']", 15, _iPad, "xpath").Click();

            // retrieve last foto from gallery and click it
			var list = _iPad.FindElementsByXPath("//*[contains(@label, 'Photo, Portrait')]");
			list[(list.Count - 1)].Click();
			                    
            // validate on iPhone
            logComment("Validate received picture on iPhone");
            findId("//*[@label='Owner, iOS']", 10, _iPhone, "xpath").Click();
			ValidateExist("(//*[@label='Heruntergeladen'])[" + pictureUploadAmount + "]", 25, _iPhone, "xpath").Click();

            // retrieve all images from chat and click the correct one
            logComment("Open received picture from iPhone");
			var list2=_iPhone.FindElementsById("_thumbnailImage");
			list2[pictureUploadAmount-1].Click();
            pictureUploadAmount++;

			// validate opened image
            logComment("Validate opened image on iPhone");
			ValidateExist("_imageView", 15, _iPhone, "id");
            findId("_closeButton", 5, _iPhone, "id").Click();
            //findId("//*[@label='Kontakte']", 5, _iPhone, "xpath").Click();
        }

		[Test]
		public void document_in_chat()
		{
			// ipad (ios.owner) send document to iphone (ios.user1)
            logComment("Sending document from iPad to iPhone");
			findId("//*[@label='User1, iOS']", 15, _iPad, "xpath").Click();
			findId("_addItemButton", 5, _iPad, "id").Click();
			findId("//*[@label='Documents']", 5, _iPad, "xpath").Click();
			findId("s7_, txt", 5, _iPad, "id").Click();

			// validate on iPhone
            logComment("validate received document on iPhone");
            findId("//*[@label='Owner, iOS']", 15, _iPhone, "xpath").Click();
			ValidateExist("(//*[@label='Heruntergeladen'])[" + pictureUploadAmount + "]", 25, _iPhone, "xpath").Click();
			ValidateLabel("_fileNameLabel", 5, _iPhone, "s7_.txt", "id");

			// retrieve all images from chat and click the correct one
            logComment("Try opening received document on iPhone");
            var list2 = _iPhone.FindElementsById("_thumbnailImage");
            list2[pictureUploadAmount - 1].Click();
            pictureUploadAmount++;
			ValidateExist("//*[@label='In Dateien sichern']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label='Abbrechen']", 5, _iPhone, "xpath").Click();
			//findId("//*[@label='Kontakte']", 5, _iPhone, "xpath").Click();
        }

		[Test]
		public void filter_contacts()
		{
            

            logComment("Filterinf contactlist on iPhone and iPad");
			findId("_searchBarContacts", 15, _iPad, "id").Click();
			findId("_searchBarContacts", 5, _iPhone, "id").Click();
			findId("_searchBarContacts", 5, _iPad, "id").SendKeys("User1");
			findId("_searchBarContacts", 5, _iPhone, "id").SendKeys("User");
			ValidateNotExist("//*[@label='User2, iOS']", 1, _iPad, "xpath");
			ValidateExist("//*[@label='User1, iOS']", 5, _iPad, "xpath");
			ValidateNotExist("//*[@label='Owner, iOS']", 1, _iPhone, "xpath");
			ValidateExist("//*[@label='User2, iOS']", 5, _iPhone, "xpath");
            findId("_searchBarContacts", 5, _iPad, "id").SendKeys("");
            findId("_searchBarContacts", 5, _iPad, "id").Clear();
            // action_iPad.LongPress(sbar).Wait(2000).Release().Perform();
			findId("//*[@label='Select All']", 5, _iPad, "xpath").Click();
			findId("//*[@label='Cut']", 5, _iPad, "xpath").Click();
            findId("_searchBarContacts", 5, _iPhone, "id").SendKeys("");
            findId("_searchBarContacts", 5, _iPhone, "id").Clear();
            // action_iPhone.LongPress(sbar).Wait(2000).Release().Perform();
			findId("//*[@label='Alles']", 5, _iPhone, "xpath").Click();
			findId("//*[@label='Ausschneiden']", 5, _iPhone, "xpath").Click();
			ValidateExist("//*[@label='Owner, iOS']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label='User2, iOS']", 5, _iPad, "xpath");
			//ValidateExist("//*[@label='User1, iOS']", 5, _iPad, "xpath").Click();
			//_iPad.HideKeyboard();
			//_iPhone.HideKeyboard("Search");

		}

		[Test]
        public void about_settings()
        {
            // validate about settings on iPad and iPhone
            logComment("Validating the About section of settings");
			findId("settingsTab", 15, _iPad, "id").Click();
			findId("settingsTab", 5, _iPhone, "id").Click();
			findId("//*[@label='About']", 5, _iPad, "xpath").Click();
			findId("//*[@label='Über']", 5, _iPhone, "xpath").Click();
			findId("//*[@label='Open source licenses']", 5, _iPad, "xpath").Click();
			findId("//*[@label='Open-Source-Lizenzen']", 5, _iPhone, "xpath").Click();
			ValidateExist("//*[@label='Open source licenses']", 5, _iPad, "xpath");
			ValidateExist("//*[@label='Open source licenses']", 5, _iPhone, "xpath");
			ValidateExist("_uiLabelTermsTitle", 5, _iPad, "id");
			ValidateExist("_uiLabelTermsTitle", 5, _iPhone, "id");
			ValidateExist("_uiLabelPrivacyTitle", 5, _iPad, "id");
			ValidateExist("_uiLabelPrivacyTitle", 5, _iPhone, "id");
			ValidateExist("_uiLabelLicenseTitle", 5, _iPad, "id");
			ValidateExist("_uiLabelLicenseTitle", 5, _iPhone, "id");
			ValidateValue("_uiTextViewTermsLink", 5, _iPad, "https://testing.beeware.de/licence_agreement", "id");
			ValidateValue("_uiTextViewTermsLink", 5, _iPhone, "https://testing.beeware.de/licence_agreement", "id");
			ValidateValue("_uiTextViewPrivacyLink", 5, _iPad, "https://testing.beeware.de/privacy", "id");
			ValidateValue("_uiTextViewPrivacyLink", 5, _iPhone, "https://testing.beeware.de/privacy", "id");
			//findId("contactsTab", 5, _iPad, "id").Click();
			//findId("//*[@label='Über']", 5, _iPhone, "xpath").Click();
			//findId("//*[@label='Einstellungen']", 5, _iPhone, "xpath").Click();
			//findId("contactsTab", 5, _iPhone, "id").Click();

        }

		[Test]
        public void video_settings()
        {
            // validate video quality settings on iPad and iPhone
            logComment("Change Videosettings on iPad and iPhone and validate the change");
            findId("settingsTab", 15, _iPad, "id").Click();
            findId("settingsTab", 5, _iPhone, "id").Click();
			findId("//*[@label='Video call and messages']", 5, _iPad, "xpath").Click();
			findId("//*[@label='Videoanruf und Nachrichten']", 5, _iPhone, "xpath").Click();
			ValidateLabel("(//*[@label = 'High'])[1]",5,_iPad,"High", "xpath").Click();
			ValidateLabel("(//*[@label = 'High'])[1]", 5, _iPhone, "High", "xpath").Click();
			ValidateExist("//*[@label='Very High']", 5, _iPad, "xpath");
			ValidateExist("//*[@label = 'High']", 5, _iPad, "xpath");
			ValidateExist("//*[@label = 'Medium']", 5, _iPad, "xpath");
			ValidateExist("//*[@label = 'Low']", 5, _iPad, "xpath");
			ValidateExist("//*[@label = 'Very High']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label = 'High']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label = 'Medium']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label = 'Low']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label = 'Ultra Low']", 5, _iPhone, "xpath").Click();
			ValidateExist("//*[@label = 'Ultra Low']", 5, _iPad, "xpath").Click();
			findId("//*[@label='About']", 5, _iPad, "xpath").Click();
			findId("//*[@label='Video call and messages']", 5, _iPad, "xpath").Click();
			ValidateLabel("(//*[@label = 'Ultra Low'])[1]", 5, _iPad, "Ultra Low", "xpath");
			findId("//*[@label='Videoanruf und Nachrichten']", 5, _iPhone, "xpath").Click();
			findId("//*[@label='Einstellungen']", 5, _iPhone, "xpath").Click();
			findId("//*[@label='Videoanruf und Nachrichten']", 5, _iPhone, "xpath").Click();
			ValidateLabel("(//*[@label = 'Ultra Low'])[1]", 5, _iPhone, "Ultra Low", "xpath");

			// validate audio quality settings on iPad and iPhone
            logComment("Change the Autiosettings on iPad and iPhone and validate the change");
			ValidateLabel("(//*[@label = 'High'])[1]", 5, _iPad, "High", "xpath").Click();
            ValidateLabel("(//*[@label = 'High'])[1]", 5, _iPhone, "High", "xpath").Click();
            ValidateExist("//*[@label='Very High']", 5, _iPad, "xpath");
            ValidateExist("//*[@label = 'High']", 5, _iPad, "xpath");
            ValidateExist("//*[@label = 'Medium']", 5, _iPad, "xpath");
            ValidateExist("//*[@label = 'Very High']", 5, _iPhone, "xpath");
            ValidateExist("//*[@label = 'High']", 5, _iPhone, "xpath");
            ValidateExist("//*[@label = 'Medium']", 5, _iPhone, "xpath");
			ValidateExist("//*[@label = 'Low']", 5, _iPhone, "xpath").Click();
			ValidateExist("//*[@label = 'Low']", 5, _iPad, "xpath").Click();
            findId("//*[@label='About']", 5, _iPad, "xpath").Click();
            findId("//*[@label='Video call and messages']", 5, _iPad, "xpath").Click();
            ValidateLabel("(//*[@name = 'Low'])[1]", 5, _iPad, "Low", "xpath");
            findId("//*[@label='Videoanruf und Nachrichten']", 5, _iPhone, "xpath").Click();
            findId("//*[@label='Einstellungen']", 5, _iPhone, "xpath").Click();
            findId("//*[@label='Videoanruf und Nachrichten']", 5, _iPhone, "xpath").Click();
            ValidateLabel("(//*[@label = 'Low'])[1]", 5, _iPhone, "Low", "xpath");
			//findId("contactsTab", 5, _iPad, "id").Click();
			//findId("//*[@label='Einstellungen']", 5, _iPhone, "xpath").Click();
			//findId("contactsTab", 5, _iPhone, "id").Click();
        }

		[Test]
        public void logout()
        {
            logComment("Logout of the client");
			findId("settingsTab", 15, _iPad, "id").Click();
            findId("settingsTab", 5, _iPhone, "id").Click();
			findId("_logOffButton", 5, _iPad, "id").Click();
			findId("_logOffButton", 5, _iPhone, "id").Click();
            logComment("Validate that the user is logged out of the client");
            ValidateValue("_username", 5, _iPad, "owner@ios.de", "id");
            ValidateValue("_username", 5, _iPhone, "user1@ios.de", "id");
            logComment("Login to Sodalis");
			findId("_password", 5, _iPad).Click();
            findId("_password", 5, _iPhone).Click();
            findId("_password", 5, _iPad).Clear();
            findId("_password", 5, _iPhone).Clear();
            findId("_password", 5, _iPad).SendKeys("x99sFUV!");
            findId("_password", 5, _iPhone).SendKeys("x99sFUV!");
            findId("Go", 5, _iPad).Click();
            findId("Go", 5, _iPhone).Click();
            logComment("Validate that the user is logged in");
			ValidateLabel("_changeStatusOnUserBtn", 15, _iPad, "Online", "id");
            ValidateLabel("_changeStatusOnUserBtn", 15, _iPhone, "Online", "id");
        }


		[OneTimeTearDown]
		public void ShutDOwn()
		{
            _iPad.CloseApp();
            _iPhone.CloseApp();
             _iPad.RemoveApp("de.beeware.sodalis.mobile");
             _iPhone.RemoveApp("de.beeware.sodalis.mobile");


			_iPad.Quit();
            _iPhone.Quit();
           
			_iPad.Dispose(); 
			_iPhone.Dispose();
			/// only be used when the appium server was started programmatically in the onetimesetup area
            //_service.Dispose();

		}
        


	}

}




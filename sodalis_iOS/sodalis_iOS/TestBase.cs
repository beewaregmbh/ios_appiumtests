﻿using System;
using System.IO;
using System.Diagnostics;

using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.iOS;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Appium.Service;
using OpenQA.Selenium.Appium.Service.Options;
using System.Runtime;
using SeleniumExtras.WaitHelpers;


namespace sodalis_iOS
{

    public class DrvWithName: IOSDriver<IWebElement>
    {
        public DrvWithName( Uri uri, DesiredCapabilities capabilities, TimeSpan timeSpan, string name)
            :base(uri, capabilities, timeSpan)
        {
            Name = name; 
        }

        public string Name { get; }
    }

    public abstract class TestBase
	{

        public static AppiumLocalService _service = new AppiumServiceBuilder().WithIPAddress("127.0.0.1").Build();
        public static DrvWithName _iPad;
        public static DrvWithName _iPhone;
		public static int pictureUploadAmount = 1;

		public static string TestFolder = TestContext.CurrentContext.TestDirectory;
        public static DirectoryInfo LogsFolder;
		public static DirectoryInfo SubTestFolder;


        public static void logComment(string comment)
        {
            TestContext.Out.WriteLine("[ Log Comment ] "+comment);
        }




        public static void closeOldSessions()
        {
            string cmd = " -c lsof -P | grep ':4723' | awk '{print $2}' | xargs kill -9";
            System.Diagnostics.Process.Start("/bin/sh", cmd);
            System.Threading.Thread.Sleep(8000);
        }

        public static OpenQA.Selenium.IWebElement waitFor(string by, int duration, string pathType, DrvWithName drv, bool assertOn=true)
		{
			OpenQA.Selenium.IWebElement result = null;
			var found = false;
			if (pathType == "id")
			{
				int i = 1;
                while (i < (duration * 5 ))
				{
					try
					{
						//WebDriverWait wait = new WebDriverWait(drv, TimeSpan.FromSeconds(duration));
						//wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id(by)));
						result=drv.FindElementById(by);
						i = (duration * 5);
						found = true;
                        TestContext.Out.WriteLine("[PASS:WaitForElement] Found Element "+by+" within "+duration+" searchcycles on device "+drv.Name);
					}
					catch (Exception)
					{
						// var found = false;
						// Assert.True(found == true, "Waited " + duration.ToString() + " seconds for Element " + by + " to be clickable. " + e);
						i++;
						found = false;
					}
				}
				if (assertOn == true)
				{
                    if(found==false)
                    {
                        TestContext.Out.WriteLine("[FAIL:WaitForElement] Element " + by + " not Found within " + duration + " searchcycles on device " + drv.Name);   
                    }
                    Assert.IsTrue(found);
				}
			}
			if (pathType == "xpath")
            {
                int i = 1;
                while (i < (duration*5))
                {
                    try
                    {
      //                  WebDriverWait wait = new WebDriverWait(drv, TimeSpan.FromSeconds(duration));
						//wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(by)));
						result = drv.FindElementByXPath(by);
						i = duration * 5;
						found = true;
                        TestContext.Out.WriteLine("[PASS:WaitForElement] Found Element " + by + " within " + duration + " searchcycles on device "+drv.Name);
                    }
                    catch (Exception)
                    {
                        // var found = false;
                        // Assert.True(found == true, "Waited " + duration.ToString() + " seconds for Element " + by + " to be clickable. " + e);
                        i++;
						found = false;
                    }
                }
				if(assertOn==true)
				{
                    if (found == false)
                    {
                        TestContext.Out.WriteLine("[FAIL:WaitForElement] Element " + by + " not Found within " + duration + " searchcycles on device " + drv.Name);
                    }
					Assert.IsTrue(found);
				}
            }
			return result;
		}
        
        public OpenQA.Selenium.IWebElement findId(string by, int duration, DrvWithName drv, string pathType="id", bool assertOn=true)
		{
            TestContext.Out.WriteLine("[FindElement] Element " + by + " will be searched on device " + drv.Name+" for further actions");
            var res=waitFor(by, duration, pathType, drv, assertOn);
            return res;
		}

        public OpenQA.Selenium.IWebElement ValidateExist(string by, int duration, DrvWithName drv, string pathType="id")
		{
			OpenQA.Selenium.IWebElement result2= null;
			int found = 1;
			try
			{
				result2=waitFor(by, duration, pathType, drv);
				found = 1;
                TestContext.Out.WriteLine("[PASS:ValidateExist] Found Element " + by + " within " + duration + " searchcycles on device " + drv.Name);
			}	
			catch(Exception)
			{
				found = 0;
            TestContext.Out.WriteLine("[FAIL:ValidateExist] Element " + by + " not Found within " + duration + " searchcycles on device " + drv.ToString());
				Assert.IsTrue(found == 1);
			}
			return result2;
		}

        public OpenQA.Selenium.IWebElement ValidateNotExist(string by, int duration, DrvWithName drv, string pathType = "id")
        {
            OpenQA.Selenium.IWebElement result4 = null;
            try
            {
                result4 = waitFor(by, duration, pathType, drv, false);
                Assert.IsNull(result4);
                TestContext.Out.WriteLine("[PASS:ValidateNotExist] Element " + by + " not Found within " + duration + " searchcycles on device " + drv.Name);
            }
            catch (Exception)
            {
                TestContext.Out.WriteLine("[FAIL:ValidateNotExist] Found Element " + by + " within " + duration + " searchcycles on device " + drv.Name);
            }
            return result4;
        }

        public OpenQA.Selenium.IWebElement ValidateLabel(string by, int duration, DrvWithName drv, string searchString, string pathType = "id")
		{
			OpenQA.Selenium.IWebElement result3 = null;
			try
			{   
				result3 = waitFor(by, duration, pathType,drv);
                var equal=result3.GetAttribute("label").Contains(searchString);
				Assert.IsTrue(equal);
                TestContext.Out.WriteLine("[PASS:ValidateLabel] Label on Element " + by + " on device " + drv.Name+" contains "+searchString);
			}
            catch(Exception)
			{
                int found = 0;
                TestContext.Out.WriteLine("[FAIL:ValidateLabel] Label on Element " + by + " on device " + drv.Name + " does not contains " + searchString);
                Assert.IsTrue(found == 1);
			}
			return result3;
		}

        public OpenQA.Selenium.IWebElement ValidateValue(string by, int duration, DrvWithName drv, string searchString, string pathType = "id")
        {
            OpenQA.Selenium.IWebElement result5 = null;
            try
            {
                result5 = findId(by, duration, drv, pathType);
                bool equal = result5.GetAttribute("value").Contains(searchString);
                Assert.IsTrue(equal);
                TestContext.Out.WriteLine("[PASS:ValidateValue] Value on Element " + by + " on device " + drv.Name + " contains " + searchString);
            }
            catch (Exception)
            {
                int found = 0;
                TestContext.Out.WriteLine("[FAIL:ValidateValue] Value on Element " + by + " on device " + drv.Name + " does not contains " + searchString);
                Assert.IsTrue(found==1);
            }
            return result5;
        }
	}
}
